<?php
session_start();

$status = isset($_SESSION['username']) ? "Welcome {$_SESSION['username']}" : 'Not logged in';

require 'views\index.view.php';
