<?php
session_start();

if (isset($_POST['email']) && isset($_POST['password'])) {

    $email = htmlspecialchars($_POST['email']);
    $password = $app['database']->getUserPassword($email);

    if (password_verify($_POST['password'], $password)) {
        $_SESSION['user'] = $app['database']->getUserId($email);
        $_SESSION['username'] = $app['database']->getUserName($email);

        header('Location: /');
    } else {
        echo 'Invalid password.';
    }
}

require 'views/login.view.php';