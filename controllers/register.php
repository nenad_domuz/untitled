<?php
session_start();

if (isset($_POST['email']) && isset($_POST['username']) && isset($_POST['password'])) {

    $email = htmlspecialchars($_POST['email']);
    $username = htmlspecialchars($_POST['username']);
    $password = password_hash($_POST['password'], PASSWORD_DEFAULT);

    if ($app['database']->isDuplicateEmail($email) || $app['database']->isDuplicateUsername($username)) {
        echo 'There is some duplicate data';
    } else {
        $app['database']->store($email,$username,$password);
        echo 'Registered successfully';
    }
}

require 'views/register.view.php';