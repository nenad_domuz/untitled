<?php

class Router
{
    /**
     * @var array
     */
    protected $routes = [];

    public static function load($file)
    {
        $router = new static;

        require $file;

        return $router;
    }
    
    /**
     * @param $routes
     */
    public function define($routes)
    {
        $this->routes = $routes;
    }

    /**
     * @param $uri
     * @return mixed
     * @throws Exception
     */
    public function direct($uri)
    {
        if(array_key_exists($uri, $this->routes)){
            return $this->routes[$uri];
        }

        throw new Exception('No Route defined');
    }
}