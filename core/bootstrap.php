<?php

$app = [];

$app['config'] = require 'config.php';

require __DIR__ . '/Request.php';
require __DIR__ . '/Router.php';
require __DIR__ . '/database/' . 'Connection.php';
require __DIR__ . '/database/' . 'QueryBuilder.php';

$app['database']  = new QueryBuilder(
    Connection::make($app['config']['database'])
);