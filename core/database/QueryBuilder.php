<?php

class QueryBuilder
{
    /**
     * @var PDO
     */
    private $pdo;

    /**
     * QueryBuilder constructor.
     * @param PDO $pdo
     */
    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @param $email
     * @param $username
     * @param $password
     */
    public function store($email, $username, $password)
    {
        $statement = $this->pdo->prepare('insert into users (email, username, password) values (:email, :username, :password)');

        $statement->bindParam(':email', $email);
        $statement->bindParam(':username', $username);
        $statement->bindParam(':password', $password);

        $statement->execute();
    }

    /**
     * @param $email
     * @return string
     */
    public function isDuplicateEmail($email)
    {
        $statement = $this->pdo->prepare('select count(*) from users where email = :email limit 1');

        $statement->bindParam(':email', $email);
        $statement->execute();

        return $statement->fetchColumn();
    }

    /**
     * @param $username
     * @return string
     */
    public function isDuplicateUsername($username)
    {
        $statement = $this->pdo->prepare('select count(*) from users where username = :username limit 1');

        $statement->bindParam(':username', $username);
        $statement->execute();

        return $statement->fetchColumn();
    }

    /**
     * @param $email
     * @return string
     */
    public function getUserName($email)
    {
        $statement = $this->pdo->prepare('select username from users where email = :email');

        $statement->bindParam(':email', $email);

        $statement->execute();

        return $statement->fetchColumn();
    }

    /**
     * @param $email
     * @return string
     */
    public function getUserPassword($email)
    {
        $statement = $this->pdo->prepare('select password from users where email = :email');

        $statement->bindParam(':email', $email);

        $statement->execute();

        return $statement->fetchColumn();
    }

    /**
     * @param $email
     * @return string
     */
    public function getUserId($email)
    {
        $statement = $this->pdo->prepare('select id from users where email = :email');

        $statement->bindParam(':email', $email);

        $statement->execute();

        return $statement->fetchColumn();
    }
}