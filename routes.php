<?php

$router->define([
    '' => 'controllers/index.php',
    'register' => 'controllers/register.php',
    'login' => 'controllers/login.php',
    'search' => 'controllers/search.php'
]);