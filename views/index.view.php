<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Home</title>
    <style>
        ul {
            padding: 0;
        }

        ul > li {
            list-style: none;
            display: block;
            padding: 10px 0;
        }
        button {
            background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            cursor: pointer;
        }

        button:hover {
            opacity: 0.8;
        }

        a, a:visited {
            color: #fff;
            text-decoration: none;
        }
        .cancelbtn {
            width: auto;
            padding: 10px 18px;
            background-color: #f44336;
        }
    </style>
</head>
<body>
    <header>
        <nav>
            <ul class="search">
                <li>
                    <form action="" method="" enctype="multipart/form-data">
                        <input type="text" placeholder="Search..." id="criteria" name="criteria">
                        <button type="submit" disabled>
                            Go
                        </button>
                    </form>
                </li>
            </ul>
            <ul class="nav">
                <li>
                    <a class="cancelbtn" href="register">Register</a>
                </li>
                <li>
                    <a class="cancelbtn" href="login">Login</a>
                </li>
            </ul>
        </nav>
    </header>
    <h2><?= $status ?></h2>
    <main>
        <h1>Welcome to home page</h1>
    </main>
</body>
</html>