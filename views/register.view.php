<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registration</title>
    <style>
        form {
            border: 3px solid #f1f1f1;
        }

        input[type=text], input[type=email], input[type=password] {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            box-sizing: border-box;
        }

        button {
            background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            cursor: pointer;
            width: 100%;
        }

        button:hover {
            opacity: 0.8;
        }

        a, a:visited {
            color: #fff;
            text-decoration: none;
        }
        .cancelbtn {
            width: auto;
            padding: 10px 18px;
            background-color: #f44336;
        }

        .container {
            padding: 16px;
        }
    </style>
</head>
<body>
    <header>
        <h1>Please fill out the form</h1>
    </header>
    <form action="" name="registration_form" method="post" enctype="multipart/form-data">
        <div class="container">
            <label for="email">Email</label>
            <input type="email" name="email" placeholder="Your email" required autocomplete="off">

            <label for="username">Username</label>
            <input type="text" name="username" placeholder="What would you like your usename to be?" required autocomplete="off">

            <label for="password">Password</label>
            <input type="password" id="password" name="password" required>

            <label for="confirm_password">Confirm password</label>
            <input type="password" id="confirm_password" name="confirm_password" required>

            <button type="submit">
                Submit
            </button>
        </div>
    </form>
    <div class="container" style="background-color:#f1f1f1">
        <a href="/" class="cancelbtn">Cancel</a>
    </div>
    <script type="text/javascript">
        var password = document.getElementById("password")
            , confirm_password = document.getElementById("confirm_password");

        function validatePassword(){
            if(password.value != confirm_password.value) {
                confirm_password.setCustomValidity("Passwords Don't Match");
            } else {
                confirm_password.setCustomValidity('');
            }
        }

        password.onchange = validatePassword;
        confirm_password.onkeyup = validatePassword;
    </script>
</body>
</html>